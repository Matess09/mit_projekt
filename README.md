# <b>🚗 Parkovací senzor</b>

Senzor slouží k lepší orientaci při parkovaní. Můj senzor se umístí na místo před jezdem do garáže a určuje, kdy má auto zastavit. Upozorňuje nás to pomocí světelného i zvukového signálu.

<br>

# 🤔<b> Jak to vlastně funguje? </b>
Můj parkovací senzor funguje na principu ultrazvukového snímání. V jednoduchosti při parkování snímač vyšle ultrazvukovou vlnu, která se odrazí od překážky zpátky k senzoru a vypočítá jeho skutečnou vzdálenost od překážky.
<br>

Většina těchto senzorů je umístěna na karosérii auta. V mém případě to bude naopak, senzor bude umístěn např. v garáži a auto se bude chovat jako překážka.
</br>
<br>

![Schéma zapojení z kiCADu](hcsr04.png "Princip funkce ultrazvuku")
(Princip funkce ultrazvukového senzoru HC-SR04)
<br>
</br>


# ⚙ Součástky
| Název                 | Popis                                           | Odkaz                                                                           | Cena    | Cena celkem   |
| ------------------    | ----------------------------------------------- |    -------------------------------------------------------------------------    | ------  | ------------- |
| STM8S                 | MCU                                             |    [farnell](https://cz.farnell.com/stmicroelectronics/nucleo-8l152r8/       )                                                                                                                                                                               | 290 Kč  |    290 Kč     | 
| HC-SR04               | Ultrazvukový měřič vzdálenosti                  |    [láskakit](https://www.laskakit.cz/arduino-ultrazvukovy-meric-vzdalenosti-hc-sr04/)              | 38 Kč   |    38  Kč     |
| HC-SR04 - Remote      | Řídící jednotka pro SR-04                       |    [láskakit](https://www.laskakit.cz/ridici-jednotka-s-displejem-pro-ultrazvukove-cidlo-hc-sr04/)  | 138 Kč  |    138 Kč     |
| Bzučák                | Akustický bzučák                                |    [láskakit](https://www.laskakit.cz/akusticky-bzucak--modul/)                                     | 38 Kč   |    38 Kč      |
| WS2812B - 64          | Inteligentní RGB Led čtverec 8x8                |    [láskakit](https://www.laskakit.cz/8x8-inteligentni-rgb-led-neopixel-ctverec--ws2812--5050--5v/)                     | 198 kč  |    198 Kč     |
|                       | <b>Cena celkem:                                 |                                                                                 |         | <b>702 Kč     |
 
<br>

----

## <b> Inteligentní RGB led Matice 8x8 (WS2812B - 64) </b>

Pro světelnou signalizaci jsem si vybral tento "lehce" programovatelnou RGB led matici. LED diody jsou typu Neopixel, což znamená, že každá LED dioda obsahuje vestavěný čip s pamětí 3 bajty. Těmi se nastavuje intenzita svitu každé ze tří barev. To umožňuje komunikaci pomocí jednokanálového rozhraní. Znamená to tedy, že můžeš řídit spoustu LED diod pomocí jediného digitálního pinu. Celkově má tři piny (Ucc, I/O, GND).

```
uint8_t redx[192]=
{
0,150,0,
} 
```
Tento kód vytvoří pattern, a vyhodnotí barvu.

<br>

## <b> Řídící jednotka pro ultrazvukový senzor HC-SR04 </b>
Tento modul nebyl k vyřešení mého projektu potřeba, ale zajímala mě jeho funkčnost, tak jsem ho do tohoto projektu přidal.

Obsahuje třímístný sedmisegmentový displej, který nám ukazuje přesnou vzdálenost senzoru a navíc má sériový port na pinech "TX" a "RX", přes který celý tento projekt funguje pomocí UART.

<br>

# 🗺️ Blokové schéma

```mermaid
flowchart TB
    USB[PC]--+5V-->MCU[STM8]
    MCU--+5V-->WS2812B-64[RGB LED Matice]
    MCU--Data-->WS2812B-64[RGB LED Matice]
    MCU--->BUZ[Bzučák]
    MCU--+5V-->RC[Řídící jednotka]
    MCU--Data-->RC[Řídící jednotka]
    RC--+5V-->HCSR04[Senzor]
    RC--Data-->HCSR04[Senzor]
```
<br>

# 📇Vývojový diagram

```mermaid
    graph LR
    A[Start] --> B(Inicializace)
    B --> C{While_1}
    C -->|Neproběhl| D[Konec]
    C -->|Proběhl| E[Změř distance]
    E --> F{If distance <= 5cm}
    F --> |Ano| G[Rozsviť X]
    F --> |Ne| H{If distance <= 15cm}
    H --> |Ano| I[Rozsviť -]
    H --> |Ne| I[Rozsviť ->]
    I --> G
    I --> C
    
    
```

<br>

#  📈Schéma zapojení

![Schéma zapojení z kiCADu](Schéma.jpg "Schéma zapojení")


<br>

# 📷Fotografie

![Schéma zapojení z kiCADu](green.jpg "Schéma zapojení")
<br>
![Schéma zapojení z kiCADu](orange.jpg "Schéma zapojení")
<br>
![Schéma zapojení z kiCADu](red.jpg "Schéma zapojení")
<br>

# 🥳Závěr
Tvoření projektu bylo velmi namáhavé, ale myslím si, že za výsledek to stálo. Nejvíce času jsem strávil nad výtvorem kódu pro vypsání 3 číslic a práce s nima. Celkový projekt mně stál cca. 700kč. Čekal jsem, že částka bude menší, ale není to nic tragického. 
<br>
Celkově mně projekt naučil mnoho. Např. práci s gitem nové funkce v Ccéčku a mnoho dalšího. Je hezké, že projekt je funkční, to mě dělá asi nejvíce štastného😄.



# 📚 Použité knihovny:

* [WS2812B-64](http://www.elektromys.eu/clanky/ele_ws2812b/clanek.html) - Elektromyš

----
<br>

 
Autorem projektu: <b>Matěj Drmola </b>




