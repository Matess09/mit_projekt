#ifndef INC_BUZZER_H
#define INC_BUZZER_H

#include "stm8s.h"

void BUZZER(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz

    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);

    while(1)
    {
    for (uint32_t i = 0; i < 100000; i++)  //změna proměnné
        ;
    GPIO_WriteReverse(GPIOC, GPIO_PIN_5);
    }
}

#endif