#include "stm8s.h"
#include <stdio.h>
#include <stdlib.h> /* atoi */
#include <string.h>

#define MAX_LEN 7  // maximální délka čteného řetězce
#define L_PULSE 6  // 6*1/16MHz = 6*62.5 = 375ns (~400ns)
#define H_PULSE 12 // 12*1/16MHz = 12*62.5 = 750ns (~800ns)

uint8_t cnt = 0;
uint32_t j = 0;
uint32_t f1 = 25;
uint32_t f2 = 2;

char received_string[MAX_LEN]; // buffer
char received_int_str[4];

void clearRXBuffer(void) // Smazání bufferu
{
  int i;
  for (i = 0; i < MAX_LEN; i++)
    received_string[i] = 0;
  for (i = 0; i < 4; i++)
    received_int_str[i] = 0;
  cnt = 0;
}

void send_char(char c)
{
  while (!UART1_GetFlagStatus(UART1_FLAG_TXE))
    ;
  UART1_SendData8(c);
}

void send_str(char str[])
{
  for (uint32_t i = 0; str[i]; i++)
    send_char(str[i]);
}

int UART_get_int(void) // načti číslo
{
  char t = 0;
  uint8_t konec = 1;

  while (konec)
  {
    while (!UART3_GetFlagStatus(UART3_FLAG_RXNE))
      ;
    t = UART3_ReceiveData8();

    if (t == 'D')
    {
      konec = 0;
    }
  }
  received_string[cnt++] = t;

  uint8_t konec1 = 1;

  while (konec1)
  {
    while (!UART3_GetFlagStatus(UART3_FLAG_RXNE))
      ;
    t = UART3_ReceiveData8();
    received_string[cnt++] = t;

    if (cnt >= MAX_LEN)
    {
      konec1 = 0;
    }
  }

  if (cnt == MAX_LEN)
  {
    // Kontrola, že řezězec má správnou strukturu
    send_str("Zpráva čidla: ");
    send_str(received_string);
    send_char('\n');
    send_char('\r');

    if ((received_string[0] == 'D') && (received_string[1] == ':') && (received_string[5] == '\r') && (received_string[6] == '\n'))
    {
      send_str(" > je spravny format");
      received_int_str[0] = received_string[2];
      received_int_str[1] = received_string[3];
      received_int_str[2] = received_string[4];
      received_int_str[3] = '\0';

      int ex = atoi(received_int_str);
      clearRXBuffer();
      return ex;
    }

    clearRXBuffer();
    return (-1);
  }

  else
  {
    clearRXBuffer();
    return (-1);
  }
}

void get_char(void)
{
  while (!UART3_GetFlagStatus(UART3_FLAG_RXNE))
    ;
  UART3_ReceiveData8();
}

void init_tim(void)
{
  TIM1_TimeBaseInit(0, TIM1_COUNTERMODE_UP, 15, 0); // Upcounting, prescaler 0, dont care period/ARR value

  TIM1_OC1Init(TIM1_OCMODE_PWM2,
               TIM1_OUTPUTSTATE_ENABLE,
               TIM1_OUTPUTNSTATE_DISABLE,
               1,
               TIM1_OCPOLARITY_HIGH,
               TIM1_OCNPOLARITY_HIGH,
               TIM1_OCIDLESTATE_SET,
               TIM1_OCNIDLESTATE_RESET);

  TIM1_CtrlPWMOutputs(ENABLE); // Timer output global enable
  TIM1_SelectOnePulseMode(TIM1_OPMODE_SINGLE);
}

// LED_number * 3 bytes  = (RGB per LED) !!
void test(uint8_t *data, uint16_t length)
{
  uint8_t mask;
  disableInterrupts(); // can be omitted if interrupts do not take more then about ~25us
  while (length)
  {
    length--;
    mask = 0b10000000; // for all bits in byte
    while (mask)
    {
      while (TIM1->CR1 & TIM1_CR1_CEN)
        ; // wait if timer run (transmitting last bit)
      if (mask & data[length])
      {
        TIM1->ARRL = H_PULSE; // set pulse width for "H" bit
      }
      else
      {
        TIM1->ARRL = L_PULSE; // set pulse width for "L" bit
      }
      TIM1->CR1 |= TIM1_CR1_CEN; // Spustí časovač
      mask = mask >> 1;
    }
  }
  enableInterrupts();
}

// test pattern
uint8_t redx[192] =
    {
      0,150,0,  //RED
      0,150,0,   
      0,0,0,    //BLACK
      0,0,0,    
      0,0,0,    
      0,0,0, 
      0,150,0,  
      0,150,0, 
      0,150,0,  
      0,150,0, 
      0,150,0, 
      0,0,0,
      0,0,0,
      0,150,0,  
      0,150,0, 
      0,150,0, 
      0,0,0, 
      0,150,0,  
      0,150,0, 
      0,150,0, 
      0,150,0,  
      0,150,0, 
      0,150,0, 
      0,0,0,  
      0,0,0, 
      0,0,0,  
      0,150,0,  
      0,150,0, 
      0,150,0, 
      0,150,0,
      0,0,0, 
      0,0,0,
      0,0,0, 
      0,0,0,
      0,150,0, 
      0,150,0, 
      0,150,0,
      0,150,0, 
      0,0,0, 
      0,0,0,
      0,0,0,
      0,150,0, 
      0,150,0,
      0,150,0, 
      0,150,0, 
      0,150,0,
      0,150,0, 
      0,0,0,
      0,150,0, 
      0,150,0,
      0,150,0, 
      0,0,0,
      0,0,0,
      0,150,0, 
      0,150,0,
      0,150,0, 
      0,150,0,
      0,150,0, 
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0,
      0,150,0,
      0,150,0,
};

uint8_t greenarrow[192] =
    {
      0,0,0,    //BLACK
      0,0,0,
      0,0,0, 
      0,0,115,  //GREEN
      0,0,115,
      0,0,0,
      0,0,0,
      0,0,0, 
      0,0,0,
      0,0,0,
      0,0,115, 
      0,0,115,
      0,0,115,
      0,0,115,
      0,0,0,
      0,0,0, 
      0,0,0,
      0,0,115,
      0,0,115, 
      0,0,115,
      0,0,115,
      0,0,115,
      0,0,115,
      0,0,0, 
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,115,
      0,0,115,
      0,0,0,
      0,0,0,
      0,0,0, 
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,115,
      0,0,115,
      0,0,0,
      0,0,0, 
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,115,
      0,0,115,
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,115,
      0,0,115,
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,115,
      0,0,115,
      0,0,0,
      0,0,0,
      0,0,0,
    };

uint8_t orange[192] =
    {
      0,0,0,
      0,0,0,
      0,0,0, 
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0, 
      0,0,0,
      0,0,0,
      0,0,0, 
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0, 
      0,0,0,
      0,0,0,
      0,0,0, 
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0,
      0,0,0, 
      0,255,69,
      0,255,69,
      0,255,69,
      0,255,69,
      0,255,69,
      0,255,69,
      0,255,69,
      0,255,69,
      0,255,69,
      0,255,69,
      0,255,69,
      0,255,69,
      0,255,69,
      0,255,69,
      0,255,69,
      0,255,69,
    };

void main(void)
{
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz
  GPIO_Init(GPIOE, GPIO_PIN_0, GPIO_MODE_OUT_PP_LOW_SLOW);
  GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);

  UART3_Init(9600, UART3_WORDLENGTH_8D, UART3_STOPBITS_1, UART3_PARITY_NO, UART3_MODE_TXRX_ENABLE);
  UART3_Cmd(ENABLE);

  UART1_Init(
      9600,                             // rychlost v Bd (Baud)
      UART1_WORDLENGTH_8D,              // počet přenášených bitů
      UART1_STOPBITS_1,                 // počet stop bitů
      UART1_PARITY_NO,                  // parita
      UART1_SYNCMODE_CLOCK_DISABLE,
      UART1_MODE_TXRX_ENABLE            // mod TX a RX
  );

  UART1_Cmd(ENABLE);

  init_tim();

  while (1)
  {

    int distance = UART_get_int();

    if (distance <= 5)
    {
      test(redx, sizeof(redx));

      while (j < f1)
      {
        for (uint32_t k = 0; k < 3000; k++)        
          ;
        GPIO_WriteReverse(GPIOE, GPIO_PIN_0);     //Bzučák
        j++;
      }
      j = 0;
    }

    else if (distance <= 15)
    {
      test(orange, sizeof(orange));
      while (j < f2)
      {
        for (uint32_t k = 0; k < 8000; k++) 
          ;
        GPIO_WriteReverse(GPIOE, GPIO_PIN_0);     //Bzučák
        j++;
      }
      j = 0;
    }

    else
    {
      test(greenarrow, sizeof(greenarrow));
    }
  }
}
